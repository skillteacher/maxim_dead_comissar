using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dangerous : MonoBehaviour
{
    [SerializeField] private int damege = 1;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        health health = collision.GetComponent<health>();
        if (health == null) return;
        health.takeDamage(damege);
     }
}
