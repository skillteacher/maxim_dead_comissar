using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desh : MonoBehaviour
{
    [SerializeField] private KeyCode dashButton = KeyCode.LeftShift;
    [SerializeField] private float dashForse;
    [SerializeField] private Collider2D physicalShape;
    [SerializeField] private movement movement;
    private Rigidbody2D rb;
    [SerializeField] private float dashDelay = 0.5f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        if(Input.GetKeyDown(dashButton)) { DoDash(); }
    }


    private void DoDash()
    {
        Vector2 velocity = new Vector2(movement.isTurnedRight ? dashForse : -dashForse, 0f);
        rb.velocity = velocity;
        movement.isDesh = true;
        StartCoroutine(DashDelay(dashDelay));
    }

    private IEnumerator DashDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        movement.isDesh = false;
    }
}
