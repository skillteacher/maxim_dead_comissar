using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class coin : MonoBehaviour
{
    [SerializeField] private int coinCost;
    private Game game;
    private bool isCollected = false;

    private void Start()
    {
        game = FindObjectOfType<Game>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    { 
        if (isCollected ||game == null) return;
        isCollected = true;
        game.AddCoint(coinCost);
        Destroy(gameObject); 
    }




}
