 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class health : MonoBehaviour
{
    [SerializeField] private int maxLives = 3;
    private Vector3 startPosition;
    private int lives;

    private void Start()
    {
        lives = maxLives;
        PlayerUI.ui.SetLives(lives);
        startPosition = transform.position;
    }

    public void takeDamage(int damage)
    {
        lives -= damage;
        PlayerUI.ui.SetLives(lives);

        if (lives <= 0)
        {
            Death();
        }
        else
        {
            transform.position = startPosition;
        }

    }

    private void Death()
    {
        Game game = FindObjectOfType<Game>();
        game.GameOver(maxLives);
    }


    







}
