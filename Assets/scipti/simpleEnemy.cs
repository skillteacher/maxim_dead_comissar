using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class simpleEnemy : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private bool isTurnedRight;
    private Rigidbody2D rb;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Vector2 velocity =
            new Vector2(isTurnedRight ? speed : -speed, rb.velocity.y);
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        Flip();
    }

    private void Flip()
    {
        isTurnedRight = !isTurnedRight;
        transform.Rotate(0f, 180f, 0f);
    }
}
