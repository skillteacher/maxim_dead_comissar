using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    public static PlayerUI ui;

    [SerializeField] private TMP_Text livesCount;
    [SerializeField] private TMP_Text coinsCount;

    private void Awake()
    {
        if (ui == null)
        {
            ui = this;
        }
        else
        {
            Destroy(this);
        }

    }


    public void SetLives(int amount)
    {
        if (amount < 0) amount = 0;
        livesCount.text = amount.ToString();
    }
    public void SetCoins(int amount)
    {
        coinsCount.text = amount.ToString();
    }
}