using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{

    [SerializeField] private GameObject restartMenu;
    [SerializeField] private TMP_Text finalScore;
    private int coinCount = 0;
    private int currentMaxLives;
    [SerializeField] string firstLVL;

    private void Awake()
    {
        Game[] anotherGameScript = FindObjectsOfType<Game>(); 
        if(anotherGameScript.Length>1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            restartMenu.SetActive(false);
        }        
    }

    public void AddCoint(int amount)
    {
        coinCount += amount;
        PlayerUI.ui.SetCoins(coinCount);
    }

    public void GameOver(int maxLives)
    {
        Time.timeScale = 0f;
        restartMenu.SetActive(true);
        finalScore.text = coinCount.ToString();
        currentMaxLives = maxLives;
    }

    public void RestartMenu()
    {
        coinCount = 0;
        PlayerUI.ui.SetLives(coinCount);
        SceneManager.LoadScene(firstLVL);
        Time.timeScale = 1f;
        restartMenu.SetActive(false);
    }
}
