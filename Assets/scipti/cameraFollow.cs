using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    private void Update()
    {
        Vector2 pos = cameraTransform.position;
        transform.position = pos;
    }
}
